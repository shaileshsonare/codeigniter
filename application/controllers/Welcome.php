<?php

/*

Ref:

http://localhost/codeigniter/
https://www.google.co.in/search?ei=cCeFWqCZIYS0jwOjs7igBQ&q=remove+first+element+from+array+php&oq=remove+first+elemen&gs_l=psy-ab.3.1.0l10.1167390.1176023.0.1177270.19.12.0.7.7.0.191.1231.0j9.9.0....0...1c.1.64.psy-ab..3.16.1346...0i67k1j0i131k1.0.29OhLBshNaE
https://artisansweb.net/read-csv-excel-file-php-using-phpspreadsheet/
https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-files/
https://stackoverflow.com/questions/33342994/unexpected-use-t-use-when-trying-to-use-composer
https://stackoverflow.com/questions/22249541/codeigniter-autoload-specific-classes
https://www.codeigniter.com/user_guide/general/autoloader.html
https://www.phpclasses.org/browse/file/61821.html
https://stackoverflow.com/questions/32339582/codeigniter-composer
https://www.google.co.in/search?ei=omaEWuClFsvevgTeg4_IDg&q=php+excel+xlsx+writer&oq=php+excel+xlsx&gs_l=psy-ab.3.2.0l2j0i22i30k1j0i22i10i30k1j0i22i30k1l5j0i22i10i30k1.409958.417962.0.419892.16.15.1.0.0.0.380.2599.0j2j6j2.10.0....0...1c.1.64.psy-ab..5.11.2623...0i67k1j0i10k1j0i13k1j0i13i30k1.0.VBqjHDQsAv4
https://phpspreadsheet.readthedocs.io/en/develop/
https://stackoverflow.com/questions/28500467/how-to-write-a-text-to-a-txt-file-codeigniter
https://www.ebay.in/itm/4-x-20mm-double-sided-suction-cups-pads-suckers-/200479428556
https://www.google.co.in/search?q=double+sided+suction+cups%2Fpads&oq=double+sided+suction+cups%2Fpads&aqs=chrome..69i57j69i60&sourceid=chrome&ie=UTF-8
https://gitlab.com/shaileshsonare/codeigniter/blob/master/application/controllers/Welcome.php
https://wordpress.org/plugins/piklist/

*/

defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function combineHeaderKeys($keys, $values) {
		$return = array();
		foreach ($values as $key => $val ) {
			$return[] = array_combine($keys, $val);
		}
		return $return;
	}
	
	public function csvReader() {
		$inputFileName = './data.csv';

		/** Create a new Xls Reader  **/
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xml();
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Ods();
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Slk();
		//    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Gnumeric();
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		/** Load $inputFileName to a Spreadsheet Object  **/
		$spreadsheet = $reader->load($inputFileName);
		
		$sheetData = $spreadsheet->getActiveSheet()->toArray();
		echo "<pre>";
		
		print_r($sheetData);

		$headers_array = array_shift($sheetData);
		
		echo "<br/>------------------------</br/>";
		
		print_r($headers_array);
		
		$csv_data = $this->combineHeaderKeys($headers_array , $sheetData);
		
		echo "<br/>------------------------</br/>";
		
		print_r($csv_data);
			
		echo "</pre>";
		
		exit;
	}
	 
	public function index()
	{
		/* //$this->load->helper('file');
		$inputFileName = "application/controllers/test.xlsx";
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
											NULL,
											TRUE,
											FALSE);
			var_dump($rowData);
			//  Insert row data array into your database of choice here
		}
	
		$objPHPExcel = new PHPExcel_Writer_Excel2007();
		$objPHPExcel->getProperties()->setTitle("title")
                 ->setDescription("description");
        
		// Assign cell values
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'cell value here');

		// Save it as an excel 2003 file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2017');
		var_dump($objWriter->save("application/controllers/nameoffile.xlsx")); */
		
		//require(APPPATH . 'vendor/autoload.php');
		
		$this->csvReader();
	 		
		$spreadsheet = new Spreadsheet();
		
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');
		$writer = new Xlsx($spreadsheet);
		$writer->save('application/controllers/hello_world.xlsx');
		
		$this->load->view('welcome_message');
	}
}
