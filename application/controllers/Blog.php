<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $blog_model = $this->load->model('Blog_model');

        $data = array("blogs" => $this->Blog_model->insert_entry());
        $this->load->view('blog_list.php', $data);
    }

    public function cc() {
        $data['cost']  = $this->input->get('cost', TRUE);
        $this->load->view('currency_converter', $data);
    }

}